
open Binding

(* Each participant in the DKG generates a secret key and public key and publishes 
   their public key in order to form a committee. 

   The committee represents the set of participants in the distributed key generation
   algorithm. Since the algorithm used is synchronous, it is paramount that there is
   an ordering, which is defined by the hash of the public key of each participant 
   (the participant identity). The hash based ordering of participants attributes a 
   position to each, such that each participant is aware of every other participants
   position and thus defines which row in a polynomial belongs to which participant.
*)

type t = {
  index_map : (int, int) Hashtbl.t ;
  public_key_map : (int, Public_key.t) Hashtbl.t ;
}

(* Construction *)

let create committee_size =
  { index_map = Hashtbl.create committee_size ;
    public_key_map = Hashtbl.create committee_size ;
  }

let length t =
  Hashtbl.length t.index_map

let generate public_keys =
  let participant_ids =
    List.stable_sort (fun (h1, _) (h2, _) -> compare h1 h2)
      (List.map
         (fun public_key ->
            let h = Hashtbl.hash public_key in
            Printf.printf "(ocaml) h = %d \n" h;
            flush stdout ;
            (Hashtbl.hash public_key, public_key))
         public_keys)
  in
  let committee = create (List.length public_keys) in
  List.iteri (fun position (participant_id, public_key) ->
      Hashtbl.add committee.index_map participant_id position ;
      Hashtbl.add committee.public_key_map participant_id public_key
    ) participant_ids ;
  committee

let generate_test committee_size =
  let rec generate_test_participants i lim acc =
    if i >= lim then
      acc
    else
      generate_test_participants (i + 1) lim (Participant.keypair () :: acc)
  in
  let test_participants = generate_test_participants 0 committee_size [] in
  let test_public_keys = List.map (fun Participant.{ public_key ; _ } ->
      public_key
    ) test_participants
  in
  let test_secret_keys = List.map (fun Participant.{ secret_key ; _ } ->
      secret_key
    ) test_participants
  in
  ( test_secret_keys, generate test_public_keys )

(* Verification *)

let check_arity t arr =
  assert (length t = Array.length arr)

(* Functions *)

let find_public_key t h =
  Hashtbl.find t.public_key_map h

let find_position t h =
  Hashtbl.find t.index_map h

let find_position_opt t h =
  Hashtbl.find_opt t.index_map h
