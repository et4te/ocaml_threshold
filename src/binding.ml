open Alien_ffi

(* Field *)

module Fr = struct
  type t

  external (=) : t -> t -> bool = "fr_eq"

  external serialize : Arena.t -> t -> Bigstring.t = "fr_serialize"
  external deserialize : Bigstring.t -> t = "fr_deserialize"
end

module G1 = struct
  type t

  external (=) : t -> t -> bool = "g1_eq"
end

module G1_affine = struct
  type t

  external one : unit -> t = "g1_affine_one"
  external mul : t -> Fr.t -> G1.t = "g1_affine_mul"
end

(* Polynomial *)

module Commitment = struct
  type t
end

module Bivariate_commitment = struct
  type t

  external row : t -> int -> Commitment.t = "bivar_commitment_row"
  external evaluate : t -> int -> int -> G1.t = "bivar_commitment_evaluate"
end

module Polynomial = struct
  type t

  external make : int -> t = "poly"

  external zero : unit -> t = "poly_zero"
  external one : unit -> t = "poly_one"

  external commitment : t -> Commitment.t = "poly_commitment"
  external evaluate : t -> int -> Fr.t = "poly_evaluate"
  external serialize : Arena.t -> t -> Bigstring.t = "poly_serialize"
  external deserialize : Bigstring.t -> t = "poly_deserialize"
end

module Bivariate_polynomial = struct
  type t

  external random : int -> t = "bivar_poly"

  external row : t -> int -> Polynomial.t = "bivar_poly_row"
  external evaluate : t -> int -> int -> Fr.t = "bivar_poly_evaluate"

  external commitment : t -> Bivariate_commitment.t = "bivar_commitment"
end

(* BLS *)

module Ciphertext = struct
  type t

  external verify : t -> bool = "ciphertext_verify"
end

module Signature = struct
  type t
end

module Public_key = struct
  type t

  external encrypt : t -> Bigstring.t -> Ciphertext.t = "encrypt"
  external verify : t -> Signature.t -> Bigstring.t -> bool = "verify"
end

module Secret_key = struct
  type t

  external generate : unit -> t = "generate"
  external public_key : t -> Public_key.t = "public_key"
  external sign : t -> Bigstring.t -> Signature.t = "sign"
  external decrypt : Arena.t -> t -> Ciphertext.t -> Bigstring.t = "decrypt"
end

(* Threshold *)

type int_bigarray = (int, Bigarray.int_elt, Bigarray.c_layout) Bigarray.Array1.t

module Signature_share = struct
  type t
end

module Public_key_share = struct
  type t

  external verify : t -> Signature_share.t -> Bigstring.t -> bool = "share_verify"
end

module Secret_key_share = struct
  type t

  external sign : t -> Bigstring.t -> Signature_share.t = "share_sign"
end

module Public_key_set = struct
  type t

  external public_key : t -> Public_key.t = "public_key_of_set"
  external share : t -> int -> Public_key_share.t = "public_key_share"
end

module Secret_key_set = struct
  type t

  external generate : int -> t = "generate_set"
  external public_keys : t -> Public_key_set.t = "public_keys"
  external share : t -> int -> Secret_key_share.t = "secret_key_share"
  external sign_shares : t -> Public_key_set.t -> int_bigarray -> Bigstring.t -> Signature.t = "sign_shares"
  external decrypt_shares : Arena.t -> t -> Public_key_set.t -> Ciphertext.t -> int_bigarray -> Bigstring.t = "decrypt_shares"
  external decrypt_verify_shares : Arena.t -> t -> Public_key_set.t -> Ciphertext.t -> int_bigarray -> Bigstring.t = "decrypt_verify_shares"
end
