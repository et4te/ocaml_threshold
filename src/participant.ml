
open Binding

(* A participant owns a secret key (here it is BLS) *)

type t = {
  secret_key : Secret_key.t ;
  public_key : Public_key.t ;
}

let keypair () =
  let secret_key = Secret_key.generate () in
  let public_key = Secret_key.public_key secret_key in
  { secret_key ; public_key }

(* TODO : Gossip participation by publishing public_key *)

let publish _t =
  assert false
