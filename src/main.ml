
open Binding

(* Wrappers for freeing external resources.
   * In the case of opaque structures (OCaml custom data structures and 
     operations), a Gc.full_major suffices in order to finalize the 
     allocated structures.
   * For externally allocated structures (bigarrays), the typed arena
     containing the allocated memory must be explicitly free'd.
*)

let with_opaques f =
  f () ; Gc.full_major ()

let with_arena f =
  let open Alien_ffi in
  let arena = Arena.alloc () in
  f arena ;
  Arena.free arena

let with_full f =
  with_opaques (fun () ->
      with_arena (fun arena -> f arena)
    )

let int_bigarray arr =
  Bigarray.Array1.of_array Bigarray.int Bigarray.c_layout arr

let run_test_sign () =
  with_opaques (fun () ->
      for i = 0 to 10 do
        let m = Bigstring.of_string (Printf.sprintf "message %d" i) in
        let sk = Secret_key.generate () in
        let pk = Secret_key.public_key sk in
        let s = Secret_key.sign sk m in
        if Public_key.verify pk s m then
          Printf.printf "Signature @ [i <- %d] = true\n" i
        else
          Printf.printf "Signature @ [i <- %d] = false\n" i
      ; flush stdout ; Gc.full_major ()
      done
    )

let run_test_encrypt () =
  with_full (fun arena ->
      for i = 0 to 10 do 
        let m = Bigstring.of_string (Printf.sprintf "message %d" i) in
        let sk = Secret_key.generate () in
        let pk = Secret_key.public_key sk in
        let c = Public_key.encrypt pk m in
        if Ciphertext.verify c then
          Printf.printf "Ciphertext @ [i <- %d] = true\n" i
        else
          Printf.printf "Ciphertext @ [i <- %d] = false\n" i ;
        let d = Secret_key.decrypt arena sk c in
        if Bigstring.compare m d = 0 then
          Printf.printf "(D = M) @ [i <- %d] = success\n" i
        else
          Printf.printf "(D = M) @ [i <- %d] = failure\n" i ;
        flush stdout ;
        Gc.full_major ()
      done
    )

let sign_verify_share sk_set pk_set i j m =
  with_opaques (fun () ->
      let sk_share = Secret_key_set.share sk_set j in
      let pk_share = Public_key_set.share pk_set j in
      let s_share = Secret_key_share.sign sk_share m in
      if Public_key_share.verify pk_share s_share m then
        Printf.printf "Share @ [i <- %d, j <- %d] = true\n" i j
      else
        Printf.printf "Share @ [i <- %d, j <- %d] = false\n" i j ;
      flush stdout ;
      let indices = int_bigarray [|5;8;7;10|] in
      let combined_s = Secret_key_set.sign_shares sk_set pk_set indices m in
      let pk_master = Public_key_set.public_key pk_set in
      if Public_key.verify pk_master combined_s m then
        Printf.printf "Combined @ [i <- %d, j <- %d] = true\n" i j
      else
        Printf.printf "Combined @ [i <- %d, j <- %d] = false\n" i j ;
      flush stdout
    )

let run_test_threshold_sign () =
  with_opaques (fun () ->
      for i = 0 to 10 do
        let m = Bigstring.of_string (Printf.sprintf "message %d" i) in
        let sk_set = Secret_key_set.generate 3 in
        let pk_set = Secret_key_set.public_keys sk_set in
        for j = 0 to 2 do
          sign_verify_share sk_set pk_set i j m
        done ; flush stdout ; Gc.full_major ()
      done
    )

let run_test_threshold_encrypt () =
  with_full (fun arena ->
      for i = 0 to 10 do
        let m = Bigstring.of_string (Printf.sprintf "message %d" i) in
        let sk_set = Secret_key_set.generate 3 in
        let pk_set = Secret_key_set.public_keys sk_set in
        let pk_master = Public_key_set.public_key pk_set in
        let c = Public_key.encrypt pk_master m in
        let indices = int_bigarray [|5;8;7;10|] in
        let d = Secret_key_set.decrypt_verify_shares arena sk_set pk_set c indices in
        if Bigstring.compare d m = 0 then
          Printf.printf "[i <- %d] Decrypt success\n" i
        else
          Printf.printf "[i <- %d] Decrypt failure\n" i ;
        flush stdout ; Gc.full_major ()
      done
    )

let run_test_dkg () =
  let configuration = Configuration.{
      committee_size = 5 ;
      byzantine_threshold = 2 ;
    } in
  let (secret_keys, committee) =
    Committee.generate_test configuration.committee_size
  in
  let _dkg_list = Dkg.create_many configuration secret_keys committee in
  ()

(* let run_test_dkg () =
 *   let dealer_num = 3 in
 *   let node_num = 5 in
 *   let byz_threshold = 2 in
 *   Printf.printf "node_num = %d\ndealer_num = %d\nbyz_threshold = %d\n"
 *     node_num dealer_num byz_threshold ;
 *   flush stdout ;
 * 
 *   let rec generate_dealers i acc =
 *     if i >= dealer_num then
 *       List.rev acc
 *     else
 *       begin
 *         let dealer = Dkg.generate_bivariate byz_threshold in
 *         generate_dealers (i + 1) (dealer :: acc)
 *       end
 *   in
 * 
 *   let rec loop = function
 *     | [] -> ()
 *     | ( { bi_poly ; bi_commit } as dealer ) :: dealers ->
 *       for m = 1 to node_num do
 *         (\* Node `m` receives its row and verifies it. *\)
 *         let row_poly = Bivariate_polynomial.row bi_poly m in
 *         let self_row = row_of_int dealer m in
 *         assert (Dkg.verify_row dealer self_row) ;
 * 
 *         (\* Node `s` receives the `s`-th value and verifies it. *\)
 *         for s = 1 to node_num do
 *           let fr = Polynomial.evaluate row_poly s in
 *           let g1 = G1_affine.mul (G1_affine.one ()) fr in
 *           assert (Bivariate_commitment.evaluate bi_commit m s = g1) ;
 *           assert (Bivariate_polynomial.evaluate bi_poly m s = fr) ;
 *           Printf.printf "Value s @ %d verified\n" s ;
 *           flush stdout ;
 *         done ;
 *         Printf.printf "Correct values for node %d\n" m
 *       done ;
 *       loop dealers
 *   in
 *   with_opaques (fun () ->
 *       Printf.printf "generating dealers ...\n" ;
 *       flush stdout ;
 *       let dealers = generate_dealers 0 [] in
 * 
 *       Printf.printf "looping ...\n" ;
 *       flush stdout ;
 * 
 *       (\* Each dealer sends row `m` to node `m` where the index starts at 1. 
 *          Do not send row 0 to anyone! The nodes verify their rows and send
 *          value `s` onto node `s`.
 *       *\)
 * 
 *       loop dealers ;
 *       Printf.printf "done ...\n" ;
 *       flush stdout ;
 *     ) *)

let () =
  (* run_test_sign () ;
   * run_test_encrypt () ;
   * run_test_threshold_sign () ;
   * run_test_threshold_encrypt () *)
  run_test_dkg ()
