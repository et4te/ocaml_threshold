
// TODO: This should be generalised to a macro.

use alien_ffi::CustomOperations;
use alien_ffi::{finalize};
use alien_ffi::{default_compare, default_compare_ext, default_hash};
use alien_ffi::{default_serialize, default_deserialize};

use threshold_crypto::{SecretKey, SecretKeySet, SecretKeyShare};

// Version strings

static SECRET_KEY_ID: &'static [u8] = b"secret_key.v0\n\0";
static SECRET_KEY_SET_ID: &'static [u8] = b"secret_key_set.v0\n\0";
static SECRET_KEY_SHARE_ID: &'static [u8] = b"secret_key_share.v0\n\0";

// Exports

pub static SECRET_KEY_OPS: CustomOperations = CustomOperations {
    identifier: SECRET_KEY_ID.as_ptr() as *const i8,
    finalize: finalize::<SecretKey>,
    compare: default_compare,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};

pub static SECRET_KEY_SET_OPS: CustomOperations = CustomOperations {
    identifier: SECRET_KEY_SET_ID.as_ptr() as *const i8,
    finalize: finalize::<SecretKeySet>,
    compare: default_compare,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};

pub static SECRET_KEY_SHARE_OPS: CustomOperations = CustomOperations {
    identifier: SECRET_KEY_SHARE_ID.as_ptr() as *const i8,
    finalize: finalize::<SecretKeyShare>,
    compare: default_compare,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};
