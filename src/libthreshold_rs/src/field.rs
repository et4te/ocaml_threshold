
// TODO: This should be generalised to a macro.

use alien_ffi::CustomOperations;
use alien_ffi::{finalize};
use alien_ffi::{default_compare, default_compare_ext, default_hash};
use alien_ffi::{default_serialize, default_deserialize};

use threshold_crypto::{G1, G1Affine, Fr};

// Version strings

static G1_ID: &'static [u8] = b"g1.v0\n\0";
static G1_AFFINE_ID: &'static [u8] = b"g1_affine.v0\n\0";
static FR_ID: &'static [u8] = b"fr.v0\n\0";

// Exports

pub static G1_OPS: CustomOperations = CustomOperations {
    identifier: G1_ID.as_ptr() as *const i8,
    finalize: finalize::<G1>,
    compare: default_compare,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};

pub static G1_AFFINE_OPS: CustomOperations = CustomOperations {
    identifier: G1_AFFINE_ID.as_ptr() as *const i8,
    finalize: finalize::<G1Affine>,
    compare: default_compare,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};

pub static FR_OPS: CustomOperations = CustomOperations {
    identifier: FR_ID.as_ptr() as *const i8,
    finalize: finalize::<Fr>,
    compare: default_compare,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};

