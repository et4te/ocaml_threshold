
// TODO: This should be generalised to a macro.

use alien_ffi::CustomOperations;
use alien_ffi::{finalize, compare};
use alien_ffi::{default_compare_ext, default_hash};
use alien_ffi::{default_serialize, default_deserialize};

use threshold_crypto::Ciphertext;

// Version strings

static CIPHERTEXT_ID: &'static [u8] = b"ciphertext.v0\n\0";

// Exports

pub static CIPHERTEXT_OPS: CustomOperations = CustomOperations {
    identifier: CIPHERTEXT_ID.as_ptr() as *const i8,
    finalize: finalize::<Ciphertext>,
    compare: compare::<Ciphertext>,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};
