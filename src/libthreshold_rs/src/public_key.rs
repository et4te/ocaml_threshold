
// TODO: This should be generalised to a macro.

use alien_ffi::CustomOperations;
use alien_ffi::{finalize, compare, compare_ext, hash};
use alien_ffi::{default_compare_ext, default_hash};
use alien_ffi::{default_serialize, default_deserialize};

use threshold_crypto::{PublicKey, PublicKeySet, PublicKeyShare};

// Version strings

static PUBLIC_KEY_ID: &'static [u8] = b"public_key.v0\n\0";
static PUBLIC_KEY_SET_ID: &'static [u8] = b"public_key_set.v0\n\0";
static PUBLIC_KEY_SHARE_ID: &'static [u8] = b"public_key_share.v0\n\0";

// Exports

pub static PUBLIC_KEY_OPS: CustomOperations = CustomOperations {
    identifier: PUBLIC_KEY_ID.as_ptr() as *const i8,
    finalize: finalize::<PublicKey>,
    compare: compare::<PublicKey>,
    compare_ext: compare_ext::<PublicKey>,
    hash: hash::<PublicKey>,
    serialize: default_serialize,
    deserialize: default_deserialize,
};

pub static PUBLIC_KEY_SET_OPS: CustomOperations = CustomOperations {
    identifier: PUBLIC_KEY_SET_ID.as_ptr() as *const i8,
    finalize: finalize::<PublicKeySet>,
    compare: compare::<PublicKeySet>,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};

pub static PUBLIC_KEY_SHARE_OPS: CustomOperations = CustomOperations {
    identifier: PUBLIC_KEY_SHARE_ID.as_ptr() as *const i8,
    finalize: finalize::<PublicKeyShare>,
    compare: compare::<PublicKeyShare>,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};
