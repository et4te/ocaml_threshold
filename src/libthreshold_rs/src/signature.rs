
// TODO: This should be generalised to a macro.

use alien_ffi::CustomOperations;
use alien_ffi::{finalize, compare};
use alien_ffi::{default_compare_ext, default_hash};
use alien_ffi::{default_serialize, default_deserialize};

use threshold_crypto::{Signature, SignatureShare};

// Version strings

static SIGNATURE_ID: &'static [u8] = b"signature.v0\n\0";
static SIGNATURE_SHARE_ID: &'static [u8] = b"signature_share.v0\n\0";

// Exports

pub static SIGNATURE_OPS: CustomOperations = CustomOperations {
    identifier: SIGNATURE_ID.as_ptr() as *const i8,
    finalize: finalize::<Signature>,
    compare: compare::<Signature>,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};

pub static SIGNATURE_SHARE_OPS: CustomOperations = CustomOperations {
    identifier: SIGNATURE_SHARE_ID.as_ptr() as *const i8,
    finalize: finalize::<SignatureShare>,
    compare: compare::<SignatureShare>,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};
