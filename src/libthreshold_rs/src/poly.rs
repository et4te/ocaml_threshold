// TODO: This should be generalised to a macro.

use alien_ffi::CustomOperations;
use alien_ffi::{finalize, compare};
use alien_ffi::{default_compare, default_compare_ext, default_hash};
use alien_ffi::{default_serialize, default_deserialize};

use threshold_crypto::poly::{Poly, BivarPoly, Commitment, BivarCommitment};

// Version strings

static POLY_ID: &'static [u8] = b"poly.v0\n\0";
static BIVAR_POLY_ID: &'static [u8] = b"bivar_poly.v0\n\0";
static COMMITMENT_ID: &'static [u8] = b"commitment.v0\n\0";
static BIVAR_COMMITMENT_ID: &'static [u8] = b"bivar_commitment.v0\n\0";

// Exports

pub static POLY_OPS: CustomOperations = CustomOperations {
    identifier: POLY_ID.as_ptr() as *const i8,
    finalize: finalize::<Poly>,
    compare: default_compare,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};

pub static BIVAR_POLY_OPS: CustomOperations = CustomOperations {
    identifier: BIVAR_POLY_ID.as_ptr() as *const i8,
    finalize: finalize::<BivarPoly>,
    compare: default_compare,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};

pub static COMMITMENT_OPS: CustomOperations = CustomOperations {
    identifier: COMMITMENT_ID.as_ptr() as *const i8,
    finalize: finalize::<Commitment>,
    compare: compare::<Commitment>,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};

pub static BIVAR_COMMITMENT_OPS: CustomOperations = CustomOperations {
    identifier: BIVAR_COMMITMENT_ID.as_ptr() as *const i8,
    finalize: finalize::<BivarCommitment>,
    compare: compare::<BivarCommitment>,
    compare_ext: default_compare_ext,
    hash: default_hash,
    serialize: default_serialize,
    deserialize: default_deserialize,
};
