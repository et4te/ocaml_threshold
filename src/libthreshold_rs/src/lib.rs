
extern crate rand;
extern crate rand_core;
extern crate alien_ffi;
extern crate threshold_crypto;

mod secret_key;
mod public_key;
mod signature;
mod ciphertext;
mod field;
mod poly;

use alien_ffi::caml_opaque;
use alien_ffi::{Uintnat, Value};
use alien_ffi::{acquire_ba_arena, arena_alloc_ba};
use alien_ffi::{ops, caml_false, caml_true, caml_to_int, caml_to_slice};

use secret_key::*;
use public_key::*;
use signature::*;
use ciphertext::*;
use field::*;
use poly::*;

use threshold_crypto::{SecretKey, PublicKey, Signature};
use threshold_crypto::{SecretKeySet, PublicKeySet};
use threshold_crypto::{SecretKeyShare, PublicKeyShare, SignatureShare};
use threshold_crypto::Ciphertext;
use threshold_crypto::{G1, G1Affine, Fr};
use threshold_crypto::pairing::{CurveAffine};
use threshold_crypto::poly::{Poly, BivarPoly, Commitment, BivarCommitment};
use threshold_crypto::serde_impl::FieldWrap;
use rand::rngs::OsRng;

use std::collections::BTreeMap;

// G1 Affine

#[no_mangle]
pub extern fn g1_affine_one(_unit: Value) -> Value {
    caml_opaque::alloc::<G1Affine>(ops(&G1_AFFINE_OPS), G1Affine::one())
}

#[no_mangle]
pub extern fn g1_affine_mul(a: Value, b: Value) -> Value {
    let a = caml_opaque::acquire::<G1Affine>(a);
    let b = caml_opaque::acquire::<Fr>(b);
    caml_opaque::alloc::<G1>(ops(&G1_AFFINE_OPS), a.mul(b.clone()))
}

#[no_mangle]
pub extern fn g1_eq(a: Value, b: Value) -> Value {
    let a = caml_opaque::acquire::<G1>(a);
    let b = caml_opaque::acquire::<G1>(b);
    if a.eq(b) {
	return caml_true();
    } else {
	return caml_false();
    }
}

#[no_mangle]
pub extern fn fr_eq(a: Value, b: Value) -> Value {
    let a = caml_opaque::acquire::<Fr>(a);
    let b = caml_opaque::acquire::<Fr>(b);
    if a.eq(b) {
	caml_true()
    } else {
	caml_false()
    }
}

#[no_mangle]
pub extern fn fr_serialize(arena: Value, fr: Value) -> Value {
    let arena = acquire_ba_arena(arena);
    let fr = caml_opaque::acquire::<Fr>(fr);
    let data = bincode::serialize(&FieldWrap(fr)).unwrap();
    arena_alloc_ba(arena, data)
}

#[no_mangle]
pub extern fn fr_deserialize(fr_ser: Value) -> Value {
    let slice = caml_to_slice::<u8>(fr_ser);
    let fr = bincode::deserialize::<FieldWrap<Fr>>(&slice)
	.unwrap()
	.into_inner();
    caml_opaque::alloc::<Fr>(ops(&FR_OPS), fr)
}

// Polynomial functions

#[no_mangle]
pub extern fn poly(degree: Value) -> Value {
    let	mut rng = OsRng::new().expect("Failed to aquire OsRng");
    let degree = caml_to_int(degree) as usize;
    caml_opaque::alloc::<Poly>(ops(&POLY_OPS), Poly::random(degree, &mut rng))
}

#[no_mangle]
pub extern fn poly_zero(_unit: Value) -> Value {
    caml_opaque::alloc::<Poly>(ops(&POLY_OPS), Poly::zero())
}

#[no_mangle]
pub extern fn poly_one(_unit: Value) -> Value {
    caml_opaque::alloc::<Poly>(ops(&POLY_OPS), Poly::one())
}

#[no_mangle]
pub extern fn poly_commitment(poly: Value) -> Value {
    let poly = caml_opaque::acquire::<Poly>(poly);
    caml_opaque::alloc::<Commitment>(ops(&COMMITMENT_OPS), poly.commitment())
}

#[no_mangle]
pub extern fn poly_evaluate(poly: Value, s: Value) -> Value {
    let poly = caml_opaque::acquire::<Poly>(poly);
    let i = caml_to_int(s);
    caml_opaque::alloc::<Fr>(ops(&FR_OPS), poly.evaluate(i))
}

#[no_mangle]
pub extern fn poly_serialize(arena: Value, poly: Value) -> Value {
    let arena = acquire_ba_arena(arena);
    let poly = caml_opaque::acquire::<Poly>(poly);
    let data = bincode::serialize(&poly).unwrap();
    arena_alloc_ba(arena, data)
}

#[no_mangle]
pub extern fn poly_deserialize(data: Value) -> Value {
    let slice = caml_to_slice::<u8>(data);
    let row: Poly = bincode::deserialize(&slice).unwrap();
    caml_opaque::alloc::<Poly>(ops(&POLY_OPS), row)
}

#[no_mangle]
pub extern fn bivar_poly(degree: Value) -> Value {
    let	mut rng = OsRng::new().expect("Failed to aquire OsRng");
    let degree = caml_to_int(degree);
    caml_opaque::alloc::<BivarPoly>(ops(&BIVAR_POLY_OPS), BivarPoly::random(degree as usize, &mut rng))
}

#[no_mangle]
pub extern fn bivar_poly_row(bivar_poly: Value, i: Value) -> Value {
    let bivar_poly = caml_opaque::acquire::<BivarPoly>(bivar_poly);
    let i = caml_to_int(i);
    caml_opaque::alloc::<Poly>(ops(&POLY_OPS), bivar_poly.row(i))
}

#[no_mangle]
pub extern fn bivar_poly_evaluate(bi_poly: Value, i: Value, j: Value) -> Value {
    let bi_poly = caml_opaque::acquire::<BivarPoly>(bi_poly);
    let i = caml_to_int(i);
    let j = caml_to_int(j);
    caml_opaque::alloc::<Fr>(ops(&FR_OPS), bi_poly.evaluate(i, j))    
}

#[no_mangle]
pub extern fn bivar_commitment(bivar_poly: Value) -> Value {
    let bivar_poly = caml_opaque::acquire::<BivarPoly>(bivar_poly);
    caml_opaque::alloc::<BivarCommitment>(ops(&BIVAR_COMMITMENT_OPS), bivar_poly.commitment())
}

#[no_mangle]
pub extern fn bivar_commitment_row(bivar_commit: Value, row: Value) -> Value {
    let bivar_commit = caml_opaque::acquire::<BivarCommitment>(bivar_commit);
    let r = caml_to_int(row);
    caml_opaque::alloc::<Commitment>(ops(&COMMITMENT_OPS), bivar_commit.row(r))
}

#[no_mangle]
pub extern fn bivar_commitment_evaluate(bi_commit: Value, i: Value, j: Value) -> Value {
    let bi_commit = caml_opaque::acquire::<BivarCommitment>(bi_commit);
    let i = caml_to_int(i);
    let j = caml_to_int(j);
    caml_opaque::alloc::<G1>(ops(&G1_OPS), bi_commit.evaluate(i, j))
}

// Primitive functions

#[no_mangle]
pub extern fn generate(_unit: Value) -> Value {
    caml_opaque::alloc::<SecretKey>(ops(&SECRET_KEY_OPS), SecretKey::random())
}

#[no_mangle]
pub extern fn public_key(sk: Value) -> Value {
    let sk = caml_opaque::acquire::<SecretKey>(sk);
    caml_opaque::alloc::<PublicKey>(ops(&PUBLIC_KEY_OPS), sk.public_key())
}

#[no_mangle]
pub extern fn sign(sk: Value, msg: Value) -> Value {
    let sk = caml_opaque::acquire::<SecretKey>(sk);
    let data = caml_to_slice::<u8>(msg);
    caml_opaque::alloc::<Signature>(ops(&SIGNATURE_OPS), sk.sign(data))
}

#[no_mangle]
pub extern fn encrypt(pk: Value, msg: Value) -> Value {
    let pk = caml_opaque::acquire::<PublicKey>(pk);
    let data = caml_to_slice::<u8>(msg);
    caml_opaque::alloc::<Ciphertext>(ops(&CIPHERTEXT_OPS), pk.encrypt(data))
}

#[no_mangle]
pub extern fn decrypt(arena: Value, sk: Value, ciphertext: Value) -> Value {
    let arena = acquire_ba_arena(arena);
    let sk = caml_opaque::acquire::<SecretKey>(sk);
    let ciphertext = caml_opaque::acquire::<Ciphertext>(ciphertext);
    let msg = sk.decrypt(ciphertext).unwrap();
    arena_alloc_ba(arena, msg)
}

#[no_mangle]
pub extern fn verify(pk: Value, sig: Value, msg: Value) -> Value {
    let pk = caml_opaque::acquire::<PublicKey>(pk);
    let sig = caml_opaque::acquire::<Signature>(sig);
    let data = caml_to_slice::<u8>(msg);
    if pk.verify(sig, data) {
	caml_true()
    } else {
	caml_false()
    }
}

#[no_mangle]
pub extern fn ciphertext_verify(ciphertext: Value) -> Value {
    let ciphertext = caml_opaque::acquire::<Ciphertext>(ciphertext);
    if ciphertext.verify() {
	caml_true()
    } else {
	caml_false()
    }
}

// Threshold functions

#[no_mangle]
pub extern fn generate_set(lim: Value) -> Value {
    let lim = caml_to_int(lim) as usize;
    let	mut rng = OsRng::new().expect("Failed to aquire OsRng");
    let sk_set = SecretKeySet::random(lim as usize, &mut rng);
    caml_opaque::alloc::<SecretKeySet>(ops(&SECRET_KEY_SET_OPS), sk_set)
}

#[no_mangle]
pub extern fn public_keys(sk_set: Value) -> Value {
    let sk_set = caml_opaque::acquire::<SecretKeySet>(sk_set);
    let pk_set = sk_set.public_keys();
    caml_opaque::alloc::<PublicKeySet>(ops(&PUBLIC_KEY_SET_OPS), pk_set)
}

#[no_mangle]
pub extern fn secret_key_share(sk_set: Value, i: Value) -> Value {
    let sk_set = caml_opaque::acquire::<SecretKeySet>(sk_set);
    let i = caml_to_int(i);
    let sk_share = sk_set.secret_key_share(i as usize);
    caml_opaque::alloc::<SecretKeyShare>(ops(&SECRET_KEY_SHARE_OPS), sk_share)
}

#[no_mangle]
pub extern fn public_key_of_set(pk_set: Value) -> Value {
    let pk_set = caml_opaque::acquire::<PublicKeySet>(pk_set);
    caml_opaque::alloc::<PublicKey>(ops(&PUBLIC_KEY_OPS), pk_set.public_key())
}

#[no_mangle]
pub extern fn public_key_share(pk_set: Value, i: Value) -> Value {
    let pk_set = caml_opaque::acquire::<PublicKeySet>(pk_set);
    let pk_share = pk_set.public_key_share(caml_to_int(i) as usize);
    caml_opaque::alloc::<PublicKeyShare>(ops(&PUBLIC_KEY_SHARE_OPS), pk_share)
}

#[no_mangle]
pub extern fn share_sign(sk_share: Value, msg: Value) -> Value {
    let sk_share = caml_opaque::acquire::<SecretKeyShare>(sk_share);
    let data = caml_to_slice::<u8>(msg);
    caml_opaque::alloc::<SignatureShare>(ops(&SIGNATURE_SHARE_OPS), sk_share.sign(data))
}

#[no_mangle]
pub extern fn share_verify(pk_share: Value, sig_share: Value, msg: Value) -> Value {
    let pk_share = caml_opaque::acquire::<PublicKeyShare>(pk_share);
    let sig_share = caml_opaque::acquire::<SignatureShare>(sig_share);
    let data = caml_to_slice::<u8>(msg);
    if pk_share.verify(&sig_share, data) {
	caml_true()
    } else {
	caml_false()
    }
}

#[no_mangle]
pub extern fn sign_shares(sk_set: Value, pk_set: Value, indices: Value, msg: Value) -> Value {
    let sk_set = caml_opaque::acquire::<SecretKeySet>(sk_set);
    let pk_set = caml_opaque::acquire::<PublicKeySet>(pk_set);
    let indices = caml_to_slice::<Uintnat>(indices);
    let data = caml_to_slice::<u8>(msg);
    let sigs: BTreeMap<_, _> = indices
	.iter()
	.map(|&i| {
	    let sig = sk_set.secret_key_share(i).sign(data);
	    (i, sig)
	})
	.collect();
    let combined_sig = pk_set.combine_signatures(&sigs)
	.expect("Signatures match");
    caml_opaque::alloc::<Signature>(ops(&SIGNATURE_SHARE_OPS), combined_sig)
}

#[no_mangle]
pub extern fn decrypt_shares(arena: Value, sk_set: Value, pk_set: Value, ciphertext: Value, indices: Value) -> Value {
    let arena = acquire_ba_arena(arena);
    let sk_set = caml_opaque::acquire::<SecretKeySet>(sk_set);
    let pk_set = caml_opaque::acquire::<PublicKeySet>(pk_set);
    let indices = caml_to_slice::<Uintnat>(indices);
    let ciphertext = caml_opaque::acquire::<Ciphertext>(ciphertext);
    let shares: BTreeMap<_, _> = indices
	.iter()
	.map(|&i| {
	    let dec_share = sk_set
		.secret_key_share(i)
		.decrypt_share(&ciphertext)
		.expect("Invalid ciphertext");
	    (i, dec_share)
	})
	.collect();
    let msg = pk_set.decrypt(&shares, &ciphertext)
	.expect("Decryption shares match");
    arena_alloc_ba(arena, msg)
}

#[no_mangle]
pub extern fn decrypt_verify_shares(arena: Value, sk_set: Value, pk_set: Value, ciphertext: Value, indices: Value) -> Value {
    let arena = acquire_ba_arena(arena);
    let sk_set = caml_opaque::acquire::<SecretKeySet>(sk_set);
    let pk_set = caml_opaque::acquire::<PublicKeySet>(pk_set);
    let indices = caml_to_slice::<Uintnat>(indices);
    let ciphertext = caml_opaque::acquire::<Ciphertext>(ciphertext);
    let shares: BTreeMap<_, _> = indices
	.iter()
	.map(|&i| {
	    let dec_share = sk_set
		.secret_key_share(i)
		.decrypt_share(&ciphertext)
		.expect("Invalid ciphertext");
	    (i, dec_share)
	})
	.collect();
    for (i, share) in &shares {
	pk_set.public_key_share(*i)
	    .verify_decryption_share(share, &ciphertext);
    }
    let msg = pk_set.decrypt(&shares, &ciphertext)
	.expect("Decryption shares match");
    arena_alloc_ba(arena, msg)
}

