(* An implementation of HybridDKG *)

open Alien_ffi
open Binding

(* State *)

type t = {
  configuration : Configuration.t ;
  index : int option ;
  secret_key : Secret_key.t ;
  committee : Committee.t ;
  contributions : (int, Contribution.t) Hashtbl.t ;
  arena : Arena.t ;
}

let create_empty_contributions c =
  Hashtbl.create c.Configuration.committee_size

let generate_random_bivariate c =
  Bivariate_polynomial.random c.Configuration.byzantine_threshold

(* Construction / Finalisation *)

let create configuration secret_key committee =
  let public_key = Secret_key.public_key secret_key in
  let index = Committee.find_position_opt committee (Hashtbl.hash public_key) in
  let t = 
    { configuration ; index ; secret_key ; committee ;
      contributions = create_empty_contributions configuration ;
      arena = Arena.alloc ()
    }
  in
  let bi_poly = generate_random_bivariate configuration in
  (t, Contribution.generate t.arena bi_poly committee)

let create_many configuration secret_keys committee =
  let rec loop acc = function
    | [] ->
      List.rev acc
    | secret_key :: secret_keys ->
      let instance = create configuration secret_key committee in
      loop (instance :: acc) secret_keys
  in loop [] secret_keys

let finalize t =
  Arena.free t.arena ;
  Gc.full_major ()

(* Mutation *)

let create_contribution t sender_index commit =
  Hashtbl.add t.contributions sender_index
    (Contribution.make commit t.configuration.committee_size)

(* Algorithm *)

(* Has this contribution been processed a priori *)
let check_duplicate t sender_index commit =
  match Hashtbl.find_opt t.contributions sender_index with
  | Some contribution ->
    (if contribution.commit = commit then
       true
     else
       assert false)
  | None ->
    false

(* Contribution handling *)

(* Once a participant receives a contribution they must verify it. *)
let verify_contribution t sender_index ({ commit ; rows } : Contribution.m1) =
  Committee.check_arity t.committee rows ;
  if check_duplicate t sender_index commit then
    None
  else
    begin
      (* The contribution is stored regardless of process type. *)
      create_contribution t sender_index commit ;
      match t.index with
      | Some index ->
        (* This process is a validator, decrypt & deserialise row *)
        let commit_row = Bivariate_commitment.row commit (index + 1) in
        let data = Secret_key.decrypt t.arena t.secret_key rows.(index) in
        let row = Polynomial.deserialize data in
        if Polynomial.commitment row <> commit_row then
          assert false
        else
          Some row
      | None ->
        (* This process is an observer - success occurs by returning None. *)
        None
    end

type approval = {
  proposer_index : int ;
  values : Ciphertext.t array ;
}

let generate_approval t Contribution.{ commit = _ ; rows = _ } row =
  let temp_arr = Array.make t.configuration.committee_size None in
  Hashtbl.iter (fun participant_id index ->
      let public_key = Committee.find_public_key t.committee participant_id in
      let fr = Polynomial.evaluate row (index + 1) in
      let fr_ser = Fr.serialize t.arena fr in
      let encrypted = Public_key.encrypt public_key fr_ser in
      temp_arr.(index) <- Some encrypted
    ) t.committee.index_map ;
  Array.map (function
      | Some x ->
        x
      | None ->
        assert false
    ) temp_arr

let handle_contribution t sender_id contribution =
  let sender_index = Committee.find_position t.committee sender_id in
  match verify_contribution t sender_index contribution with
  | Some row ->
    let values = generate_approval t contribution row in
    Some { proposer_index = sender_index ; values }
  | None ->
    None

(* Approval handling *)

let find_contribution t proposer_index =
  Hashtbl.find t.contributions proposer_index

let approve t contribution (sender_index : int) =
  if List.mem sender_index contribution.Contribution.verified_ids then
    None
  else
    begin
      contribution.verified_ids <- sender_index :: contribution.verified_ids ;
      Hashtbl.add t.contributions sender_index contribution ;
      Some sender_index
    end

let handle_approval t sender_id ({ proposer_index ; values } as _approval) =
  let sender_index = Committee.find_position t.committee sender_id in
  Committee.check_arity t.committee values ;
  let contribution = find_contribution t proposer_index in
  match approve t contribution sender_index with
  | Some sender_index ->
    (match t.index with
     | Some index ->
       (* We are a validator, decrypt & deserialise & compare commitment *)
       let fr_ser = Secret_key.decrypt t.arena t.secret_key values.(index) in
       let fr = Fr.deserialize fr_ser in
       let g1 = Bivariate_commitment.evaluate contribution.commit (index + 1) (sender_index + 1) in
       if g1 <> (G1_affine.mul (G1_affine.one ()) fr) then
         (* fail with approval fault - Fr commitment *)
         assert false
       else
         begin
           Hashtbl.add contribution.values sender_index fr ;
           Hashtbl.add t.contributions sender_index contribution ;
           ()
         end
     | None ->
       (* We are an observer, nothing left to do *)
       ())
  | None ->
    (* We have already approved this contribution *)
    ()

