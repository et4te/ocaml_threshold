
open Binding

type t = {
  commit : Bivariate_commitment.t ;
  (** The proposers' commitment *)
  values : (int, Fr.t) Hashtbl.t ;
  (** The verified values received from approval messages for this contribution *)
  mutable verified_ids : int list ;
  (** The process ids of those who have verified this contribution *)
}

let make commit committee_size =
  { commit ; values = Hashtbl.create committee_size ; verified_ids = [] }

let is_complete t threshold =
  List.length t.verified_ids > 2 * threshold

(* Each dealer creates a public contribution comprised of a public commitment to the bivariate polynomial of that dealer and a series of encrypted rows destined to be sent to individual members of the committee, indexed positionally. *)

type m1 = {
  commit : Bivariate_commitment.t ;
  rows : Ciphertext.t array ;
}

let generate arena bi_poly committee =
  (* For each member in the committee, encrypt a row from our polynomial to be sent to the respective member using that members public key. *)
  let rows =
    Hashtbl.fold (fun participant_id position arr ->
        (* We must ensure here that 0 is never sent. *)
        Printf.printf "Participant id = %d, position = %d\n" participant_id position ;
        flush stdout ;
        let row_i = Bivariate_polynomial.row bi_poly (position + 1) in
        let data = Polynomial.serialize arena row_i in
        let public_key = Committee.find_public_key committee participant_id in
        Array.concat [ arr; [| (Public_key.encrypt public_key data) |] ]
      ) committee.index_map [||]
  in
  { commit = Bivariate_polynomial.commitment bi_poly ; rows }
