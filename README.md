
This is a binding to the `threshold_crypto` Rust library.

Due to an issue with dune linking, in order to compile the library one must point dune to the relevant location of certain static files.

In `src/dune`:
```
(data_only_dirs libthreshold_rs)

(executable
 (name main)
 (public_name threshold)
 (modules main binding)
 (libraries alien_ffi bigstring unix threads)
 (link_deps libthreshold_rs.a)
 (link_flags -linkall /path/to/ocaml_threshold/src/libthreshold_rs/target/debug/libocaml_threshold.a /path/to/.opam/default/lib/alien_ffi/c/libalien_ffi_c_stubs.a))
```